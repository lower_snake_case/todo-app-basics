import { Component, OnInit } from '@angular/core';
import { trigger, transition, style, animate } from '@angular/animations';

import { Todo } from '../../interfaces/todo';

@Component({
  selector: 'todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss'],
  animations: [
    trigger('fade', [
      transition(':enter', [
        style({ opacity: 0, transform: 'translateY(30px)' }),
        animate(200, style({ opacity:1, transform: 'translateY(0px)'}))
      ]),
      transition(':leave', [
        animate(200, style({ opacity: 0, transform: 'translateY(30px)' }))
      ])
    ])
  ]
})
export class TodoListComponent implements OnInit {

  todos: Todo[];
  todoId: number;
  todoTitle: string;
  beforeEditCache: string;
  filter: string;

  constructor() { }

  ngOnInit() {

    this.todos = [
      {
        'id': 1,
        'title': 'Finish Angular Screencast',
        'completed': false,
        'editing': false,
      },
      {
        'id': 2,
        'title': 'Take over world',
        'completed': false,
        'editing': false,
      },
      {
        'id': 3,
        'title': 'One more thing',
        'completed': false,
        'editing': false,
      },
    ];

    this.todoId          = this.todos.length;
    this.todoTitle       = '';
    this.beforeEditCache = '';
    this.filter          = 'all';
  }

  addTodo (): void {
    if (this.todoTitle.length === 0) {
      return;
    }

    this.todoId += 1;

    this.todos.push(
      {
        'id': this.todoId,
        'title': this.todoTitle,
        'completed': false,
        'editing': false
      }
    );

    this.todoTitle = '';

  }

  deleteTodo (id: number): void {
    this.todos = this.todos.filter(todo => todo.id !== id);
  }

  editTodo(todo): void {
    this.beforeEditCache = todo.title;
    todo.editing = true;
  }

  cancelEdit(todo: Todo): void {
    todo.title   = this.beforeEditCache;
    todo.editing = false;
  }

  doneEdit(todo: Todo): void {
    todo.editing = false;
  }

  changeCompleted (todo: Todo): void {
    todo.completed = !todo.completed;
  }

  remaining (): number {
    return this.todos.filter(todo => !todo.completed).length;
  }

  atLeastOneCompleted (): boolean {
    return this.todos.filter(todo => todo.completed).length > 0;
  }

  clearCompleted (): void {
    this.todos = this.todos.filter(todo => !todo.completed);
  }

  checkAllTodos (): void {
    this.todos.forEach(todo => todo.completed =
    (<HTMLInputElement>event.target).checked);
  }

  todosFiltered() : Todo[] {
    switch (this.filter) {
      case 'all':
        return this.todos;
        break;
      case 'active':
        return this.todos.filter(todo => !todo.completed);
        break;

      default:
        return this.todos.filter(todo => todo.completed);
        break;
    }

    return this.todos; // For 'all';
  }

}
